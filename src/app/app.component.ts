import { NgIf } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ElementRef, OnDestroy, ViewChild
} from '@angular/core';
import { filter, Subscription } from 'rxjs';
import { DebugInfosComponent } from './components/debug-infos/debug-infos.component';
import { FitmixWidgetService } from './fitmix-widget.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [DebugInfosComponent, NgIf],
  templateUrl: './app.component.html',
})
export class AppComponent implements AfterViewInit, OnDestroy {
  @ViewChild('inputVideo') inputVideo!: ElementRef;
  @ViewChild('drawCanvas', { static: false }) drawCanvas!: ElementRef;
  title = 'custom-grabber';
  countdown = 15;
  isProfilingComplete = false;
  subscriptions = new Subscription();
  constructor(public fitmix: FitmixWidgetService) {}

  ngAfterViewInit(): void {
    this.fitmix.loadFitMix();
    this.videoStart();
  }

  videoStart() {
    this.setupVideoCanvas();

    const interval = setInterval(() => {
      this.countdown--;
    }, 1000);

    setTimeout(() => {
      clearInterval(interval);
      window.fitmixInstance.completeProfile();
      this.isProfilingComplete = true;
    }, this.countdown * 1000);
  }

  setupVideoCanvas(): void {
    this.inputVideo.nativeElement.play();

    const fps = 40;
    const ctx = this.drawCanvas.nativeElement.getContext('2d')!;
    // flip the image horizontally in the canvas
    // because VTO adv will also flip the image
    // but we want to keep the video as it is
    ctx.translate(640, 0);
    ctx.scale(-1, 1);

    this.fitmix.fitmixIsReady.pipe(filter(Boolean)).subscribe(() => {
      this.extractImage(ctx, fps);
    });
  }

  private extractImage(ctx: CanvasRenderingContext2D, fps: number): void {
    const startExtractImage = new Date();

    // we need to display the video from any dimension in a 640x480 canvas
    // so we need to scale the video to fit the canvas and not mess up the aspect ratio

    const videoRatio =
      this.inputVideo.nativeElement.videoWidth /
      this.inputVideo.nativeElement.videoHeight;
    const canvasRatio =
      this.drawCanvas.nativeElement.width /
      this.drawCanvas.nativeElement.height;

    if (videoRatio > canvasRatio) {
      // video is wider than canvas
      const videoScaledWidth =
        this.inputVideo.nativeElement.videoWidth *
        (this.drawCanvas.nativeElement.height /
          this.inputVideo.nativeElement.videoHeight);
      const videoScaledHeight = this.drawCanvas.nativeElement.height;
      const videoScaledX =
        (this.drawCanvas.nativeElement.width - videoScaledWidth) / 2;
      const videoScaledY = 0;
      ctx.drawImage(
        this.inputVideo.nativeElement,
        videoScaledX,
        videoScaledY,
        videoScaledWidth,
        videoScaledHeight
      );
    } else {
      // video is taller than canvas
      const videoScaledWidth = this.drawCanvas.nativeElement.width;
      const videoScaledHeight =
        this.inputVideo.nativeElement.videoHeight *
        (this.drawCanvas.nativeElement.width /
          this.inputVideo.nativeElement.videoWidth);
      const videoScaledX = 0;
      const videoScaledY =
        (this.drawCanvas.nativeElement.height - videoScaledHeight) / 2;
      ctx.drawImage(
        this.inputVideo.nativeElement,
        videoScaledX,
        videoScaledY,
        videoScaledWidth,
        videoScaledHeight
      );
    }

    const imageData = ctx.getImageData(0, 0, 640, 480);

    this.fitmix.setVideoImage(imageData);

    const endExtractImage = new Date();
    const elapsedExtractImage =
      endExtractImage.getDate() - startExtractImage.getDate();

    setTimeout(() => {
      this.extractImage(ctx, fps);
    }, 1000 / fps - elapsedExtractImage);
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
