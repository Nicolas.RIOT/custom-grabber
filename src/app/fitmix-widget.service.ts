import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, timestamp } from 'rxjs';

declare global {
  interface Window {
    FitMix: any;
    fitmixInstance: any;
  }
}
window.FitMix = window.FitMix || {};

export const environment = {
  production: false,
  fitmixIntegrationLibraryUrl:
    'https://static.fittingbox.com/api/v1/fitmix.js',
  fitMixParams: {
    apiKey: 'VlteDyf5S7bspswQDQp2GKl3VIfLcNytFCtyODyy',
    sku: '561898',
    videoGrabber: true,
    debugSession: {
      name: 'test001',
      showBanner: '',
      debugFBxLive: true,
      fullDebug: true,
    },
    uiConfiguration: {
      hideEverything: true,
  },
  },
};

type DebugInfo = {
  name: string;
  timestamp?: Date;
  delta?: number;
}

@Injectable({
  providedIn: 'root',
})
export class FitmixWidgetService {
  videoGrabber: boolean = false;
  fitmixIsReady: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  fitmixIsReady$: Observable<boolean> = this.fitmixIsReady.asObservable();
  debugSession: BehaviorSubject<DebugInfo[]> = new BehaviorSubject<DebugInfo[]>([
    { name: "website loaded", timestamp: new Date(), delta: 0 },
  ]);
  debugSession$: Observable<DebugInfo[]> = this.debugSession.asObservable();

  loadFitMix(): void {
    let script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = environment.fitmixIntegrationLibraryUrl;
    script.onload = () => {
      window.fitmixInstance = window.FitMix.createWidget(
        'fitmix-container',
        {
          ...environment.fitMixParams,
          onCompleteProfile: this.onProfileData.bind(this),
        },

        () => {
          this.fitmixIsReady.next(true);
        }
      );
    };
    document.getElementsByTagName('head')[0].appendChild(script);
  }

  onProfileData(data: any[]) {
    let previousTimestamp: Date | undefined = undefined;
    data = [...this.debugSession.getValue(), ...data].map(
      (unformattedDebugInfo): DebugInfo => {
        const timestampValue =
          unformattedDebugInfo.properties?.timestamp ||
          unformattedDebugInfo.timestamp;
        const timestamp = timestampValue ? new Date(timestampValue) : undefined;
        const mappedData = {
          name: [
            unformattedDebugInfo.debugType || unformattedDebugInfo.name,
            ...(unformattedDebugInfo.event ? [unformattedDebugInfo.event] : []),
          ].join(' '),
          timestamp,
          delta:
            previousTimestamp && timestamp
              ? timestamp.getTime() - previousTimestamp.getTime()
              : undefined,
        };
        mappedData.timestamp && (previousTimestamp = mappedData.timestamp);
        return mappedData;
      }
    );
    this.debugSession.next(data);
  }

  setVideoImage(imageData: ImageData) {
    window.fitmixInstance.setVideoImage({
      data: imageData,
      dataType: 'Uint8ClampedArray',
    });
  }
}
