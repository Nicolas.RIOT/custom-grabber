import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FitmixWidgetService } from 'src/app/fitmix-widget.service';
import { Subscription } from 'rxjs';
import exportFromJSON from 'export-from-json';

@Component({
  selector: 'app-debug-infos',
  standalone: true,
  imports: [CommonModule],
  template: `
    <button
      (click)="downloadJson()"
      *ngIf="(debugSession$ | async)?.length; else wait"
      class="block mx-auto bg-gray-200 hover:bg-gray-300 text-gray-800 font-bold py-2 px-4 rounded"
    >
      Download
    </button>
    <pre *ngIf="(debugSession$ | async)?.length" class="bg-gray-100 m-5 overflow-x-auto">{{ debugSession$ | async | json }}</pre>
    <ng-template #wait>
      <p class="w-fit mx-auto">please wait...</p>
    </ng-template>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DebugInfosComponent implements OnDestroy {
  @ViewChild('data') data: any;
  subscriptions = new Subscription();
  debugSession$ = this.fitmix.debugSession$;

  constructor(public fitmix: FitmixWidgetService) {}

  downloadJson() {
    const fileName = 'download';
    const exportType = 'json';

    this.subscriptions.add(
      this.debugSession$.subscribe((data) => {
        exportFromJSON({ data, fileName, exportType });
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
